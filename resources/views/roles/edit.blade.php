<div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Edit Role</b></h4>
                </div>
                    {!! Form::model($role, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['roles.update', $role->id]]) !!}
                <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="employee" class="col-sm-3 control-label">Role Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" value="{{$role->name}}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selectPermission" class="col-sm-3 control-label">Select Permission</label>
                            <div class="col-sm-9">
                                 @foreach($permission as $value)
                                    <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                    {{ ucwords(str_replace("-"," ",$value->name)) }}</label>
                                <br/>
                                @endforeach
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i
                            class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-primary btn-flat submitbtn"><i
                            class="fa fa-save"></i> Save</button>
                </div>
            </form>
    </div>
</div>