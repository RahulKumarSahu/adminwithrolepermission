@extends('layouts.backend.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Role
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Role</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <a href="#addRolenew" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i
                                    class="fa fa-plus"></i> New</a>
                        </div>
                        <div class="box-body">
                            <table id="roleDatatable" class="table table-bordered data-table">
                                <thead>
                                    <th>Name</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Add -->
    <div class="modal fade" id="addRolenew">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Add Role</b></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" data-parsley-validate method="POST" action="{{ route('roles.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="employee" class="col-sm-3 control-label">Role Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selectPermission" class="col-sm-3 control-label">Select Permission</label>
                            <div class="col-sm-9">
                               
                                @foreach ($permission as $value)
                                    <label>{{ Form::checkbox('permission[]', $value->id, false, ['class' => 'name required-checkbox']) }}
                                        {{ ucWords(str_replace('-', ' ', $value->name)) }}</label>
                                    <br />
                                @endforeach
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i
                            class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-primary btn-flat submitbtn" name="add"><i
                            class="fa fa-save"></i> Save</button>
                     </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="showRole"></div>
    <div class="modal fade" id="editRole"></div>
@endsection


@push('scripts')
 <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
 <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet"> 
 <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript">
  $(function () {
    
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
             var table = $('#roleDatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
        url: "{{ route('rolesAjax') }}",
        type:"POST",
        data:{
            from_date: $('input[name=from_date]').val(),
            end_date: $('input[name=end_date]').val(),
            status: $('select[name=status]').val(),
            search :$('input[name=name]').val(),
        },
        dataSrc: "data"
    },
    paging: true,
    pageLength: 50,
    "bServerSide": true,
    "bLengthChange": false,
    'searching': false,
    "aoColumns": [
        { "data": "name" },
        { "data": "action" },
    ],
    "columnDefs":[
        {"targets": [0],"orderable": false},
    ]
});
             $(document).on('click','.deleteRecord',function(){
                var route = $(this).data('url');
                
                $.ajax({
                    type: "POST",
                    url: route,
                    data: '',
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function(response) {
                       if(response.status==true){
                        table.ajax.reload();
                       }
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
            });

 });
</script>
@endpush
