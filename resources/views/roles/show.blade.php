<div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Show Role</b></h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label for="employee" class="col-sm-3 control-label">Role Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" value="{{$role->name}}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selectPermission" class="col-sm-3 control-label">Select Permission</label>
                            <div class="col-sm-9">
                                 {{rtrim($role->permission,",")}}
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i
                            class="fa fa-close"></i> Close</button>
                </div>
    </div>
</div>