<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo (!empty($user['photo'])) ? '../images/'.$user['photo'] : asset('backend/images/male6.jpg'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user['firstname'].' '.$user['lastname']; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">REPORTS</li>
        <li class=""><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="header">MANAGE</li>

        @role('super-admin')
        <li class=""><a href="{{route('role.index')}}"><i class="fa fa-dashboard"></i> <span>Roles</span></a></li>
        <li class=""><a href="{{route('permission.index')}}"><i class="fa fa-dashboard"></i> <span>Permission</span></a></li>
        <li class=""><a href="{{route('user.index')}}"><i class="fa fa-dashboard"></i> <span>Users</span></a></li>
        @endrole
{{--
                <li {{ request()->is('roles') || request()->is('roles/*') ? 'active' : '' }}><a href="{{url('roles')}}"><i class="fa fa-calendar"></i> <span>Role</span></a></li>
                <li><a href="{{url('products')}}"><i class="fa fa-calendar"></i> <span>Product</span></a></li>
                <li {{ request()->is('users') || request()->is('users/*') ? 'active' : '' }}><a href="{{url('users')}}"><i class="fa fa-calendar"></i> <span>User Management</span></a></li> --}}


        {{-- <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Employees</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="employee.php"><i class="fa fa-circle-o"></i> Employee List</a></li>
            <li><a href="overtime.php"><i class="fa fa-circle-o"></i> Overtime</a></li>
            <li><a href="cashadvance.php"><i class="fa fa-circle-o"></i> Cash Advance</a></li>
            <li><a href="schedule.php"><i class="fa fa-circle-o"></i> Schedules</a></li>
          </ul>
        </li> --}}
        {{-- <li><a href="deduction.php"><i class="fa fa-file-text"></i> Deductions</a></li>
        <li><a href="position.php"><i class="fa fa-suitcase"></i> Positions</a></li>
        <li class="header">PRINTABLES</li>
        <li><a href="payroll.php"><i class="fa fa-files-o"></i> <span>Payroll</span></a></li>
        <li><a href="schedule_employee.php"><i class="fa fa-clock-o"></i> <span>Schedule</span></a></li> --}}
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
