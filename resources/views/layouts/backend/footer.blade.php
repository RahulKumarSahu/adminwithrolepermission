<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Brought To You By <a href="https://code-projects.org/">Code-Projects</a></b>
    </div>
    <strong>Copyright &copy; {{{date('Y')}}} Attendance and Payroll System </strong>
</footer>
