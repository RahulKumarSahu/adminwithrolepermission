<!-- jQuery 3 -->
<script src="{{asset('/backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('/backend/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- DataTables -->
<!-- <script src="{{asset('/backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('/backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
{{-- <script src="{{asset('/backend/bower_components/raphael/raphael.min.js')}}"></script> --}}
{{-- <script src="{{asset('/backend/bower_components/morris.js/morris.min.js')}}"></script> --}}
<!-- ChartJS -->
{{-- <script src="{{asset('/backend/bower_components/chart.js/Chart.js')}}"></script> --}}
<!-- Sparkline -->
{{-- <script src="{{asset('/backend/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script> --}}
<!-- jvectormap -->
{{-- <script src="{{asset('/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script> --}}
<!-- jQuery Knob Chart -->
{{-- <script src="{{asset('/backend/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script> --}}
<!-- daterangepicker -->
<script src="{{asset('/backend/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('/backend/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{asset('/backend/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('/backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/backend/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{asset('/backend/dist/js/pages/dashboard.js')}}"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('/backend/dist/js/demo.js')}}"></script> --}}
@stack('scripts')
<script>

  function editRecord($this){
    var route = $this.getAttribute('data-url');
    var id = $this.getAttribute('data-id');
    var model = $this.getAttribute('data-model');
    AjaxCall(route,model,'GET','html');
  }

  function showRecord($this){
    var route = $this.getAttribute('data-url');
    var id = $this.getAttribute('data-id');
    var model = $this.getAttribute('data-model');
    AjaxCall(route,model,'GET','html');
  }

  function deleteRecord($this){
    var route = $this.getAttribute('data-url');
    var id = $this.getAttribute('data-id');
    var model = $this.getAttribute('data-model');
    AjaxCall(route,model,'POST','JSON');
  }

  function AjaxCall(route,modelId,method,dataType){
    $('#'+modelId).modal('show');
    // Make an AJAX POST request
        $.ajax({
            type: method,
            url: route,
            data: '',
            dataType: dataType,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
              if(dataType=='html')
              {
                $('#'+modelId).html(response);
              }
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });


     }

  $(document).ready(function() {

  $(".editRecord1").on("click", function(e) {
    alert("First alert: Click event detected."); // Debugging line 1

    e.preventDefault();

    alert("Second alert: Default action prevented."); // Debugging line 2

    modelId = $(this).attr("data-model");
    route = $(this).attr("data-url");

    alert("Third alert: Model ID - " + modelId + ", Route - " + route); // Debugging line 3

    AjaxCall(route, modelId, 'GET');
  });
});






$(function(){
  /** add active class and stay opened when selected */
  var url = window.location;

  // for sidebar menu entirely but not cover treeview
  $('ul.sidebar-menu a').filter(function() {
     return this.href == url;
  }).parent().addClass('active');

  // for treeview
  $('ul.treeview-menu a').filter(function() {
     return this.href == url;
  }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');



});

</script>

