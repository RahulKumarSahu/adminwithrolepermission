<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.backend.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <?php
            $user = Auth()->user();
            ?>
        @include('layouts.backend.header',compact('user'))
        @include('layouts.backend.sidebar',compact('user'))
    @include('layouts.backend.messsage')
        @yield('content')
    </div>
    @include('layouts.backend.footer')

</body>

@include('layouts.backend.script')
</html>
