{{-- <x-app-layout> --}}
@extends('layouts.backend.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Create New Role</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('role.index') }}"> Back</a>
                </div>
            </div>
        </section>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="row">
                        <form method="POST" action="{{ route('role.store') }}">
                            @csrf
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label for="name"
                                    class="block font-medium text-sm text-gray-700{{ $errors->has('name') ? ' text-red-400' : '' }}">Name:</label>
                                <input id="name"
                                    class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 block mt-1 w-full{{ $errors->has('name') ? ' border-red-400' : '' }}"
                                    type="text" name="name" value="{{ old('name') }}" />
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Permissions</strong>
                                    <div class="grid grid-cols-4 gap-4">
                                        @forelse ($permissions as $permission)
                                            <div class="col-span-4 sm:col-span-2 md:col-span-1">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="permissions[]"
                                                        value="{{ $permission->name }}"
                                                        class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                                                    {{ $permission->name }}
                                                </label>
                                            </div>
                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 ">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>

@endsection

