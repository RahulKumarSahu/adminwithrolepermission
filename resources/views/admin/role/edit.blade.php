@extends('layouts.backend.app')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Role</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('role.index') }}"> Back</a>
            </div>
        </div>
    </section>

    {{-- @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif --}}


	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
				{{-- <div class="px-6 flex justify-between items-center">
					@if ($errors->any())
					<ul class="mt-3 list-none list-inside text-sm text-red-400">
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
					@endif
				</div> --}}

					<form method="POST" action="{{ route('role.update', $role->id) }}">
						@csrf
						@method('PUT')
						<div class="py-2">
							<label for="name" class="block font-medium text-sm text-gray-700{{$errors->has('name') ? ' text-red-400' : ''}}">{{ __('Name') }}</label>

							<input id="name" class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 block mt-1 w-full{{$errors->has('name') ? ' border-red-400' : ''}}" type="text" name="name" value="{{ old('name', $role->name) }}" />
						</div>
						@unless ($role->name == env('APP_SUPER_ADMIN', 'super-admin'))
						<div class="py-2">
                        <strong>Permissions</strong>
							<div class="grid grid-cols-4 gap-4">
								@forelse ($permissions as $permission)
								<div class="col-span-4 sm:col-span-2 md:col-span-1">
									<label class="form-check-label">
										<input type="checkbox" name="permissions[]" value="{{ $permission->name }}" {{ in_array($permission->id, $roleHasPermissions) ? 'checked' : '' }} class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
										{{ $permission->name }}
									</label>
								</div>
								@empty
								----
								@endforelse
							</div>
						</div>
						@endunless
						{{-- <div class="flex justify-end mt-4">
							<button type='submit' class='inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150'>
								{{ __('Update') }}
							</button>
						</div> --}}

                        <div class="col-xs-12 col-sm-12 col-md-12 ">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

