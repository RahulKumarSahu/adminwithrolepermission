@extends('layouts.backend.app')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Role
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Role</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @can('role create')
                    <div class="box-header with-border">
                        <a href="{{ route('role.create') }}" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i
                            class="fa fa-plus"></i> New</a>
                            </div>
                            @endcan

                            @if(session()->has('message'))
                        <div class="mb-8 text-green-400 font-bold">
                            {{ session()->get('message') }}
                        </div>
                        @endif

                        {{-- <form method="GET" action="{{ route('role.index') }}">
                            <div class="py-2 flex">
                                <div class="overflow-hidden flex pl-4">
                                    <input type="search" name="search" value="{{ request()->input('search') }}" class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="Search">
                                    <button type='submit' class='ml-4 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150'>
                                        {{ __('Search') }}
                                    </button>
                                </div>
                            </div>
                        </form> --}}
                    <div class="box-body">
                        <table id="roleDatatable" class="table table-bordered data-table">
                            <thead>
                                <th>Name</th>
                                @canany(['role edit', 'role delete'])
                                <th>Action</th>
                                @endcanany

                            </thead>
                            <tbody>

                                @foreach($roles as $role)
                                    <tr>
                                        <td class="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400">
                                            <div class="text-sm text-gray-900">
                                                <a href="{{route('role.show', $role->id)}}" class="no-underline hover:underline text-cyan-600 dark:text-cyan-400">{{ $role->name }}</a>
                                            </div>
                                        </td>
                                        @canany(['role edit', 'role delete'])
                                        <td class="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400">
                                            <form action="{{ route('role.destroy', $role->id) }}" method="POST">
                                                @can('role edit')
                                                <a href="{{route('role.edit', $role->id)}}" class="px-4 py-2 text-white mr-4 bg-blue-600">
                                                    {{ __('Edit') }}
                                                </a>
                                                @endcan
                                                @can('role delete')
                                                @csrf
                                                @method('DELETE')
                                                <button class="px-4 py-2 text-white bg-red-600">
                                                    {{ __('Delete') }}
                                                </button>
                                                @endcan
                                            </form>
                                        </td>
                                        @endcanany
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        {{ $roles->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
