<div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Edit Products</b></h4>
                </div>
                    {!! Form::model($products, ['method' => 'PATCH','class'=>'form-horizontal','route' => ['products.update', $products->id]]) !!}
                <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="employee" class="col-sm-3 control-label">Products Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" value="{{$products->name}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="employee" class="col-sm-3 control-label">detail</label>
                            <div class="col-sm-9">
                                <textarea  class="form-control" id="detail" name="detail" required>{{$products->detail}}</textarea>
                            </div>
                        </div>
               </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i
                            class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-primary btn-flat submitbtn"><i
                            class="fa fa-save"></i> Save</button>
                </div>
            </form>
    </div>
</div>